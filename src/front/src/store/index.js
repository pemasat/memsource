import { configureStore } from '@reduxjs/toolkit';
import testReducer from './test';
import projectsReducer from './projects';

const store = configureStore({
  reducer: {
    test: testReducer,
    projects: projectsReducer,
}});

export default store;
