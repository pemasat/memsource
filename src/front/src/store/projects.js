import { createSlice } from '@reduxjs/toolkit';

import api from '../helpers/api';
import { backendDomain } from '../const';

export const projectsSlice = createSlice({
  name: 'projects',
  initialState: {
    items: [],
    selectedProject: undefined,
  },
  reducers: {
    projectsLoadSuccess: (state, action) => {
      state.items = action.payload.items;
    },
    selectedProjectLoad: (state, action) => {
      state.selectedProject = action.payload.selectedProject;
    },
  },
});

export const { projectsLoadSuccess, selectedProjectLoad } = projectsSlice.actions;

export const loadProjects = () => async dispatch => {
  try {
    const projectResponse = await api.get(`${backendDomain}/projects`);
    const items = projectResponse.data?._embedded?.projects;
    console.log({items});
    dispatch(projectsLoadSuccess({items}));
  } catch (e) {
    return console.error(e.message);
  }
};

export const loadProjectItemAsSelected = ({id}) => async dispatch => {
  try {
    const projectItemResponse = await api.get(`${backendDomain}/projects/${id}`);
    const selectedProject = projectItemResponse.data;
    console.log({selectedProject});
    dispatch(selectedProjectLoad({selectedProject}));
  } catch (e) {
    return console.error(e.message);
  }
};



export default projectsSlice.reducer
