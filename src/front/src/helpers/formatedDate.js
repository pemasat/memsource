import { format } from 'date-fns';

export const formatedDate = (dateString) => 
  format(new Date(dateString), "y-MM-dd");
