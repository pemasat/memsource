import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import CreateEditSkeleton from '../CreateEditSkeleton';

import { loadProjectItemAsSelected } from '../../../store/projects';

const EditProject = () => {

  const dispatch = useDispatch();
  const params = useParams();
  const { id } = params;

  const projectItems = useSelector(state => state.projects.items);

  const selectedProject = useSelector(state => state.projects.selectedProject);

  useEffect(() => {
    if(!id || id === "") return;
    dispatch(loadProjectItemAsSelected({id}));
  }, []);


  const handleUpdate = (projectItem) => {
    console.log("handleUpdate", {projectItem});
  };

  if(!selectedProject) return <p>Načítám</p>;

  return (<div>
    <CreateEditSkeleton
      projectItem={selectedProject}
      handleSave={handleUpdate}
      submitButtonText="Create"
    />

  </div>);
};

export default EditProject;
