import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';

const projectsCellTitles = [
  { id: 'id', label: 'ID', useForSort: true, },
  { id: 'name', label: 'Name', useForSort: true, },
  { id: 'dateCreated', label: 'Created', useForSort: false, },
  { id: 'dateUpdated', label: 'Updated date', useForSort: false, },
  { id: 'dateDue', label: 'Due date', useForSort: true, },
  { id: 'status', label: 'Status', useForSort: true, },
];

const ProjectsTableHead = ({
  classes,
  onRequestSort,
  order,
  orderBy,
}) => {

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {projectsCellTitles.map((titleHeadItem) => (
          <TableCell
            key={titleHeadItem.id}
          >
            {titleHeadItem.useForSort
              ?
                <TableSortLabel
                  active={orderBy === titleHeadItem.id}
                  direction={orderBy === titleHeadItem.id ? order : 'asc'}
                  onClick={createSortHandler(titleHeadItem.id)}
                >
                  {titleHeadItem.label}
                  {orderBy === titleHeadItem.id ? (
                    <span className={classes.visuallyHidden}>
                      {' '}
                      ({order === 'desc' ? 'sorted descending' : 'sorted ascending'})
                    </span>
                  ) : null}
                </TableSortLabel>
              : <>{titleHeadItem.label}</>
            }
          </TableCell>
        ))}
        <TableCell />
      </TableRow>
    </TableHead>
  );
};

ProjectsTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
};

export default ProjectsTableHead;
