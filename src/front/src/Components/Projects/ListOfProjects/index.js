import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';

import { Link } from "react-router-dom";


import { formatedDate } from '../../../helpers/formatedDate';
import { getComparator, stableSort } from '../../../helpers/sort';
import ProjectsTableHead from './ProjectsTableHead';

const useStyles = makeStyles({
  table: {
    // minWidth: 650,
  },
});


const ListOfProjects = () => {
  const classes = useStyles();

  const projectItems = useSelector(state => state.projects.items);

  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('id');

  console.log({projectItems});

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="List of projects">
      <ProjectsTableHead
        {...{classes, order, orderBy}}
        onRequestSort={handleRequestSort}
      />
        <TableBody>
          {stableSort(projectItems, getComparator(order, orderBy))
            .map((projectItem) => (
            <TableRow key={projectItem.id}>
              <TableCell>{projectItem.id}</TableCell>
              <TableCell>{projectItem.name}</TableCell>
              <TableCell>{formatedDate(projectItem.dateCreated)}</TableCell>
              <TableCell>{formatedDate(projectItem.dateUpdated)}</TableCell>
              <TableCell>{formatedDate(projectItem.dateDue)}</TableCell>
              <TableCell>{projectItem.status}</TableCell>
              <TableCell><Link to={`/edit-project/${projectItem.id}`}>edit</Link></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};



export default ListOfProjects;
