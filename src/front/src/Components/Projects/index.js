import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { useHistory } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { loadProjects } from '../../store/projects';
import ListOfProjects from './ListOfProjects';

const Projects = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(loadProjects());
  }, []);

  return (<div>
    <Typography variant="h1" component="h1">Projects</Typography>
    <ListOfProjects />
    <Button variant="contained" color="secondary"
      onClick={() => {history.push('/create-project')}}
    >
      Create new
    </Button>
  </div>);
};

export default Projects;
