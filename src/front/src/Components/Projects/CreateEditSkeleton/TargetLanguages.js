import TextField from '@material-ui/core/TextField';

const TargetLanguages = ({items}) => {
  return (<div>
      {items.map(langItem =>
        <TextField
          name="targetLanguages[]"
          label="Target language"
          defaultValue={langItem}
          key={langItem}
        />
      )}
      <TextField
        name="targetLanguages[]"
        label="Target language"
        defaultValue=""
      />
    </div>);
};

export default TargetLanguages;
