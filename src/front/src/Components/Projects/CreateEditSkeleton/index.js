import { useHistory } from 'react-router-dom';
import { useRef } from 'react';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';

import TargetLanguages from './TargetLanguages';

const CreateEditSkeleton = ({
  handleSave,
  projectItem,
  submitButtonText,
}) => {
  const formRef = useRef(null);
  const history = useHistory();;

  const handleSubmit = (event) => {
    const targetLanguages =
      Array.prototype.slice.call(formRef.current.querySelectorAll("input[name='targetLanguages[]']"))
      .filter(langItem => langItem.value !== "")
      .map(langItem => langItem.value);

    handleSave({
      id: projectItem.id,
      name: formRef.current.name.value,
      sourceLanguage: formRef.current.sourceLanguage.value,
      targetLanguages,
    });
  };



  return (<div>
    <form noValidate autoComplete="off" ref={formRef}>
      <TextField id="name" label="Name" defaultValue={projectItem.name} />
      <TextField id="sourceLanguage" label="Source language" defaultValue={projectItem.sourceLanguage} />
      <TargetLanguages items={projectItem.targetLanguages} />
      <Button variant="contained" color="secondary"
        onClick={() => {history.push("/")}}
      >Cancel</Button>
      <Button variant="contained" color="primary"
        onClick={handleSubmit}
      >{submitButtonText}</Button>
    </form>
  </div>)
};

CreateEditSkeleton.prototype = {
  handleSave: PropTypes.func.isRequired,
  projectItem: PropTypes.object.isRequired,
  submitButtonText: PropTypes.string.isRequired,
};

export default CreateEditSkeleton;
