import CreateEditSkeleton from '../CreateEditSkeleton';



const CreateProject = () => {

  const handleCreate = ({projectItem}) => {
    console.log("handleCreate");
  };

  return (<div>
    <CreateEditSkeleton
      projectItem={{
        name: "",
        sourceLanguage: "",
      }}
      handleSave={handleCreate}
      submitButtonText="Create"
    />
  </div>);
};

export default CreateProject;
