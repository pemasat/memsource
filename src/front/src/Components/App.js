import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';


import Projects from './Projects';
import CreateProject from './Projects/CreateProject';
import EditProject from './Projects/EditProject';


function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/create-project"><CreateProject /></Route>
          <Route path="/edit-project/:id"><EditProject /></Route>
          <Route><Projects /></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
